CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE users (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4() NOT NULL,
    username VARCHAR(64) NOT NULL,
    email VARCHAR(64) NOT NULL,
    registered_at TIMESTAMP WITH TIME ZONE NOT NULL,
    first_name TEXT,
    surname TEXT,
    groups TEXT[] NOT NULL
);

CREATE TYPE task_level AS ENUM ('easy', 'medium', 'hard', 'any');

CREATE TABLE tasks (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4() NOT NULL,
    number_ INTEGER UNIQUE NOT NULL,
    name VARCHAR(64) NOT NULL,
    level task_level NOT NULL,
    description TEXT NOT NULL,
    code_template TEXT NOT NULL,
    public_tests TEXT NOT NULL,
    private_tests TEXT NOT NULL,
    tags TEXT[] NOT NULL,
    hints TEXT[] NOT NULL,
    expected_solution_time INTERVAL NOT NULL,
    author VARCHAR(64) NOT NULL
);

CREATE TYPE task_status AS ENUM ('not_attempted', 'in_progress', 'done', 'any');

CREATE TABLE task_statuses (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4() NOT NULL,
    task_id uuid NOT NULL,
    user_id uuid NOT NULL,
    status task_status,
    UNIQUE (task_id, user_id),
    CONSTRAINT fk_task_id FOREIGN KEY(task_id) REFERENCES tasks(id) ON DELETE NO ACTION,
    CONSTRAINT fk_user_id FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE NO ACTION
);

CREATE TYPE submissions_tests_return_codes AS ENUM ('ok', 'wrong_answer', 'time_limit_exceeded', 'memory_limit_exceeded', 'presentation_error', 'runtime_error', 'compile_time_error');

CREATE TABLE submissions_tests_public (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4() NOT NULL,
    return_code submissions_tests_return_codes NOT NULL,
    testing_output TEXT NOT NULL,
    total_tests_number INTEGER,
    first_error_test_number INTEGER
);

CREATE TABLE submissions_tests_private (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4() NOT NULL,
    return_code submissions_tests_return_codes NOT NULL,
    total_tests_number INTEGER,
    first_error_test_number INTEGER
);

CREATE TYPE submission_status AS ENUM ('rejected', 'waiting', 'testing', 'passed', 'failed');

CREATE TABLE submissions (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4() NOT NULL,
    task_id uuid NOT NULL,
    user_id uuid NOT NULL,
    submitted_at TIMESTAMP WITH TIME ZONE NOT NULL,
    tested_at TIMESTAMP WITH TIME ZONE,
    user_code TEXT,
    status submission_status,
    public_tests_id uuid,
    private_tests_id uuid,
    CONSTRAINT fk_public_tests_id FOREIGN KEY(public_tests_id) REFERENCES submissions_tests_public(id) ON DELETE NO ACTION,
    CONSTRAINT fk_private_tests_id FOREIGN KEY(private_tests_id) REFERENCES submissions_tests_private(id) ON DELETE NO ACTION
);


INSERT INTO tasks (id, number_, name, level, description, code_template, public_tests, private_tests, tags, hints, expected_solution_time, author) VALUES (
  '156eb858-5300-42ec-ae2e-37dde30448e1',
  1,
  '#1 kNN Decision.',
  'easy',
  'Implement k-nearest neighbor regression decision for given array of k nearest neighbors and point.',
  '#python code template',
  '#python code public tests',
  '#python code private tests',
  '{"knn", "regression"}',
  '{}',
  interval '20 min',
  'John Doe'
);

INSERT INTO tasks (id, number_, name, level, description, code_template, public_tests, private_tests, tags, hints, expected_solution_time, author) VALUES (
  '1411c339-d3a1-4432-9207-0f373fe99ef1',
  2,
  '#2 k-d tree.',
  'medium',
  'Implement K-d tree.',
  '#python code template',
  '#python code public tests',
  '#python code private tests',
  '{"knn", "data structures", "k-d tree"}',
  '{}',
  interval '30 min',
  'John Doe'
);

INSERT INTO users (id, username, email, registered_at, first_name, surname, groups) VALUES (
  'cff595ed-9e5d-4605-bb54-54afc71503d9',
  'test_user',
  '',
  '2021-02-23 23:50:18.720958+00',
  'test',
  'user',
  '{}'
);

INSERT INTO users (id, username, email, registered_at, first_name, surname, groups) VALUES (
  'd976e300-14ea-4588-be57-3cf876a24371',
  'another_test_user',
  '',
  '2021-02-23 23:50:18.720958+00',
  'qwerty',
  'qwerty',
  '{}'
);

INSERT INTO task_statuses (id, task_id, user_id, status) VALUES (
  '03e49cc8-062c-4ec6-8c23-325f3ba4da61',
  '156eb858-5300-42ec-ae2e-37dde30448e1',
  'cff595ed-9e5d-4605-bb54-54afc71503d9',
  'not_attempted'
);

INSERT INTO task_statuses (id, task_id, user_id, status) VALUES (
  '2a2aaa42-cdf5-43e8-bf85-3b45889b259f',
  '1411c339-d3a1-4432-9207-0f373fe99ef1',
  'cff595ed-9e5d-4605-bb54-54afc71503d9',
  'done'
);

INSERT INTO task_statuses (id, task_id, user_id, status) VALUES (
  'fb8a4f46-a602-4fd2-9224-ac001caa47c7',
  '156eb858-5300-42ec-ae2e-37dde30448e1',
  'd976e300-14ea-4588-be57-3cf876a24371',
  'not_attempted'
);

INSERT INTO task_statuses (id, task_id, user_id, status) VALUES (
  '2b23bce1-8423-4569-9f39-1f2b44871899',
  '1411c339-d3a1-4432-9207-0f373fe99ef1',
  'd976e300-14ea-4588-be57-3cf876a24371',
  'in_progress'
);

INSERT INTO submissions_tests_public (id, return_code, testing_output, total_tests_number, first_error_test_number) VALUES (
  'a142615b-f3b9-4ab9-abae-07910d0fed7a',
  'ok',
  '123',
  5,
  NULL
);

INSERT INTO submissions (id, task_id, user_id, submitted_at, tested_at, user_code, status, public_tests_id, private_tests_id) VALUES (
  '14eed0b6-803d-4cef-9e2a-4123144cbfdb',
  '1411c339-d3a1-4432-9207-0f373fe99ef1',
  'cff595ed-9e5d-4605-bb54-54afc71503d9',
  '2021-03-14 13:50:18.720958+00',
  '2021-03-14 13:52:01.720958+00',
  'print(123)',
  'passed',
  'a142615b-f3b9-4ab9-abae-07910d0fed7a',
  NULL
);

