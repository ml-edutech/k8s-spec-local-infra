# Поднимаем среду

## Установка k8s

### Для MacOS:

Требуется установка только [Docker Desktop](https://docs.docker.com/docker-for-mac/install/)

Не забудьте включить kubernetes в настройках

### Для Linux:
Устанавливаем k3s:
```sh
curl -sfL https://get.k3s.io | sh -
sudo chmod u+s $(which k3s)
```
Если после этого запустится k3s, его необходимо остановить.

После этого запускаем k3s server с параметром docker:
```sh
sudo k3s server --docker
```

## Установка kustomize
https://kubectl.docs.kubernetes.io/installation/kustomize/
> Для MacOS:
> 
>```sh
>brew install kustomize
>```

## Запуск инфраструктуры

```
kustomize build infra | kubectl apply -f -
```

## Настройка postgres
```
kubectl exec -it postgres-0 -- sh
psql -U pguser
# скопировать содержимое create_tables.sql
```


## Сборка и запуск контейнеров
В директории сервиса запустить:
```
make docker
make deploy
```

## Полезные команды
```
# статус подов
watch kubectl get pods

# сброс настроек k3s
sudo rm /etc/rancher/k3s -rf
sudo rm /var/lib/rancher/k3s -rf
```
